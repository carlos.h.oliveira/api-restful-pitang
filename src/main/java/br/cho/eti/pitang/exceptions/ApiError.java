package br.cho.eti.pitang.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * DefaultError
 */
@Getter
@Setter
@AllArgsConstructor
class ApiError {

	private Integer errorCode;

	private String message;
}