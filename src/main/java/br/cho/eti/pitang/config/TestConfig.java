package br.cho.eti.pitang.config;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import br.cho.eti.pitang.config.init.InitialData;

@Configuration
@Profile("test")
public class TestConfig {

	@Autowired
	private InitialData initialData;

	@Bean
	public boolean instantiateDatabase() throws ParseException {
		initialData.instantiateTestDatabase();

		return true;
	}

}
