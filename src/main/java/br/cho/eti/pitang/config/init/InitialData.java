/**
 * 
 */
package br.cho.eti.pitang.config.init;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.cho.eti.pitang.domain.Car;
import br.cho.eti.pitang.domain.User;
import br.cho.eti.pitang.services.UserService;

/**
 * @author Carlos H. de Oliveira - carlos.h.oliveira@cho.eti.br
 *
 */
@Component
public class InitialData {

	@Autowired
	private UserService service;

	public void instantiateTestDatabase() {

		User user1 = User.builder()//
				.birthday(LocalDate.of(1984, 4, 4))//
				.email("carlos.h.oliveira@cho.eti.br")//
				.firstName("Carlos")//
				.lastName("Oliveira")//
				.login("choliveira")//
				.password("123456")//
				.phone("81996697724")//
				.build();

		User user2 = User.builder()//
				.birthday(LocalDate.of(1987, 4, 20))//
				.email("waleska.a.silva@cho.eti.br")//
				.firstName("Waleska")//
				.lastName("Silva")//
				.login("waleskaandrade")//
				.password("123456")//
				.phone("81983158592")//
				.build();

		Car kwid = Car.builder()//
				.color("White")//
				.licensePlate("PCZ-3969")//
				.model("Renault Kwid")//
				.user(user1)//
				.yearBuild(2018)//
				.build();

		Car celta = Car.builder()//
				.color("Vermelho")//
				.licensePlate("PGI-6307")//
				.model("Celta LS")//
				.user(user1)//
				.yearBuild(2012)//
				.build();

		Car fiesta = Car.builder()//
				.color("Prata")//
				.licensePlate("PGC-0000")//
				.model("Ford Fiesta")//
				.user(user2)//
				.yearBuild(2011)//
				.build();

		user1.setCars(new HashSet<>(Arrays.asList(kwid, celta)));

		user2.setCars(new HashSet<>(Arrays.asList(fiesta)));

		service.save(user1);
		service.save(user2);
	}

}
