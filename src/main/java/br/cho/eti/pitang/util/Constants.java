/**
 * 
 */
package br.cho.eti.pitang.util;

/**
 * @author Carlos H. de Oliveira - carlos.h.oliveira@cho.eti.br
 *
 */
public abstract class Constants {

	public static final String MISSING_FIELDS = "Missing fields";

	public static final String INVALID_FIELDS = "Invalid fields";

}
