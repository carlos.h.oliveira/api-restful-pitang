/**
 * 
 */
package br.cho.eti.pitang.repositories;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.cho.eti.pitang.domain.Car;

/**
 * @author Carlos H. de Oliveira - carlos.h.oliveira@cho.eti.br
 *
 */
@Repository
public interface CarRepository extends JpaRepository<Car, String> {

	/**
	 * Find a car the car id and user id
	 * 
	 * @param id
	 * @param idUser
	 * @return an instance of Car
	 */
	Car findByIdAndUserId(String id, String idUser);

	/**
	 * Find a set of cars by the user login
	 * 
	 * @param userLogin
	 * @return a set of cars
	 */
	Set<Car> findAllByUserLogin(String userLogin);

	/**
	 * Delete a car by user id
	 * 
	 * @param idUser
	 */
	void deleteByUserId(String idUser);

	/**
	 * This method finds the quantity of license plate by license plate
	 * 
	 * @param licensePlate
	 * @return quantity
	 */
	Integer countByLicensePlateIgnoreCase(String licensePlate);

	/**
	 * This method finds the quantity of license plate by license plate and others
	 * users
	 * 
	 * @param licensePlate
	 * @param userLogin
	 * @return quantity
	 */
	Integer countByLicensePlateIgnoreCaseAndUserLoginNot(String licensePlate, String userLogin);

	/**
	 * Find a car by car id and user login
	 * 
	 * @param id
	 * @param userLogin
	 * @return an instance of Car
	 */
	Car findByIdAndUserLogin(String id, String userLogin);

	/**
	 * Find a car by license plate
	 * 
	 * @param licensePlate
	 * @return
	 */
	Car findByLicensePlateIgnoreCase(String licensePlate);
}
