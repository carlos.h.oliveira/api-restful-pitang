/**
 * 
 */
package br.cho.eti.pitang.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.cho.eti.pitang.domain.User;

/**
 * @author Carlos H. de Oliveira - carlos.h.oliveira@cho.eti.br
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, String> {

	/**
	 * Count of e-mail
	 * 
	 * @param email
	 * @return quantity
	 */
	Integer countByEmail(String email);

	/**
	 * Count of login
	 * 
	 * @param login
	 * @return quantity
	 */
	Integer countByLogin(String login);

	/**
	 * Find an user by login
	 * 
	 * @param login
	 * @return an instance of User
	 */
	User findByLogin(String login);

	/**
	 * Returns the user's password
	 * 
	 * @param username
	 * @return the password
	 */
	@Query("SELECT u.password FROM User u WHERE login = ?1")
	String findPassword(String username);

	int countByLoginAndIdNot(String email, String id);

	int countByEmailAndIdNot(String email, String id);

}
