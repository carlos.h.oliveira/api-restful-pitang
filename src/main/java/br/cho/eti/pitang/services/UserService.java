/**
 * 
 */
package br.cho.eti.pitang.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import br.cho.eti.pitang.domain.User;
import br.cho.eti.pitang.repositories.UserRepository;

/**
 * @author Carlos H. de Oliveira - carlos.h.oliveira@cho.eti.br
 *
 */
public interface UserService {

	/**
	 * Find all users
	 * 
	 * @see {@link UserRepository#findAll()}
	 */
	List<User> findAll();

	/**
	 * Find an user by user id
	 * 
	 * @see {@link UserRepository#findById()}
	 */
	User findById(String id);

	/**
	 * Returns a number of login by user login
	 * 
	 * @see {@link UserRepository#countByLogin()}
	 */
	Integer countByLogin(String login);

	/**
	 * Returns a number of login by user login
	 * 
	 * @see {@link UserRepository#countByEmail()}
	 */
	Integer countByEmail(String email);

	/**
	 * Save an user and returns a new instance Save an user and returns a new
	 * instance
	 * 
	 * @see {@link UserRepository#save()}
	 */
	User save(User user);

	/**
	 * Update an user and returns the instance
	 * 
	 * @see {@link UserRepository#findAll()}
	 */
	User update(User user, String id);

	/**
	 * Delete an user by user id
	 * 
	 * @see {@link UserRepository#findAll()}
	 */
	void delete(String id);

	/**
	 * Checks if an user exists by user id
	 * 
	 * @see {@link UserRepository#findAll()}
	 */
	void existsById(String id);

	/**
	 * Checks if an user exists by user login
	 * 
	 * @see {@link UserRepository#findAll()}
	 */
	void existsByLogin(String login);

	/**
	 * Save an image by user id and return the user
	 */
	User saveImage(MultipartFile file, String id);

	/**
	 * Authenticate the user and returns the token
	 */
	String signin(User user);

	/**
	 * Find an user by login
	 * 
	 * @see {@link UserRepository#findByLogin()}
	 */
	User findByLogin(String login);

	/**
	 * Decrease the user count when a car is updated
	 */
	void decreaseCounter(User user, Integer counter);

	/**
	 * Increase the user count when a car is user
	 */
	void increaseCounter(User user, Integer count);

}
