/**
 * 
 */
package br.cho.eti.pitang.services;

import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import br.cho.eti.pitang.domain.Car;
import br.cho.eti.pitang.domain.User;
import br.cho.eti.pitang.exceptions.AuthorizationException;
import br.cho.eti.pitang.exceptions.EntityAlreadyExistsException;
import br.cho.eti.pitang.exceptions.EntityNotFoundException;
import br.cho.eti.pitang.exceptions.FileException;
import br.cho.eti.pitang.exceptions.MissingFieldsException;
import br.cho.eti.pitang.repositories.UserRepository;
import br.cho.eti.pitang.security.JWTUtil;
import br.cho.eti.pitang.util.Constants;
import br.cho.eti.pitang.util.Util;

/**
 * @author Carlos H. de Oliveira - carlos.h.oliveira@cho.eti.br
 *
 */
@Service
class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository repository;

	@Autowired
	private CarService carService;

	@Autowired
	private ImageService imageService;

	@Autowired
	private JWTUtil jwtUtil;

	@Value("${img.prefix.user.profile}")
	private String prefix;

	@Value("${img.profile.size}")
	private Integer size;

	@Override
	public List<User> findAll() {
		return repository.findAll(Sort.by(Direction.DESC, "counter").and(Sort.by(Direction.ASC, "login")));
	}

	@Override
	public User findById(String id) {

		return repository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException(String.format("User id '%s' doesn't exist", id)));
	}

	@Transactional
	@Override
	public User save(User source) {
		Assert.isNull(source.getId(), "The user id must be null.");
		checkFields(source);

		if (!StringUtils.hasText(source.getId())) {
			source.setId(UUID.randomUUID().toString());
		}

		if (countByEmail(source.getEmail()) > 0) {
			throw new EntityAlreadyExistsException("Email already exists");
		}

		if (countByLogin(source.getLogin()) > 0) {
			throw new EntityAlreadyExistsException("Login already exists");
		}

		source.getCars().stream().forEach(car -> {
			carService.existsByLicensePlate(car.getLicensePlate());
			car.setId(UUID.randomUUID().toString());
			car.setUser(source);
		});

		source.setPassword(JWTUtil.encodePassword(source.getPassword()));

		return repository.save(source);
	}

	@Transactional
	@Override
	public User update(User source, String id) {
		User target = findById(id);

		if (repository.countByEmailAndIdNot(source.getEmail(), id) > 0) {
			throw new EntityAlreadyExistsException("Email already exists");
		}

		if (repository.countByLoginAndIdNot(source.getEmail(), id) > 0) {
			throw new EntityAlreadyExistsException("Login already exists");
		}

		Util.copyNonNullProperties(source, target);

		return repository.save(target);
	}

	@Transactional
	@Override
	public void delete(String id) {

		existsById(id);

		repository.deleteById(id);
	}

	@Override
	public void existsById(String id) {

		if (!repository.existsById(id)) {
			throw new EntityNotFoundException(String.format("User id '%s' doesn't exist", id));
		}
	}

	@Override
	public void existsByLogin(String login) {
		if (countByLogin(login) == 0) {
			throw new EntityNotFoundException(String.format("User login '%s' doesn't exist", login));
		}
	}

	@Override
	public Integer countByLogin(String login) {

		return repository.countByLogin(login);
	}

	@Override
	public Integer countByEmail(String email) {

		return repository.countByEmail(email);
	}

	@Override
	public User saveImage(MultipartFile file, String id) {
		String originalFileName = StringUtils.cleanPath(file.getOriginalFilename());
		try {
			User user = findById(id);

			if (originalFileName.contains("..")) {
				throw new FileException("The file contains invalid characters");
			}

			BufferedImage jpgImage = imageService.getJpgImageFromFile(file);
			jpgImage = imageService.cropSquare(jpgImage);
			jpgImage = imageService.resize(jpgImage, size);

			String fileName = prefix + user.getId() + ".jpg";

			user.setFile(fileName);

			InputStream inputStream = imageService.getInputStream(jpgImage, "jpg");

			user.setImg(new byte[inputStream.available()]);

			return repository.save(user);
		} catch (Exception e) {
			throw new FileException(String.format("The file %s cannot be saved", originalFileName), e);
		}
	}

	@Override
	public String signin(User user) {
		String passwordRecoverd = repository.findPassword(user.getUsername());

		if (StringUtils.isEmpty(passwordRecoverd)) {
			throw new AuthorizationException("Invalid e-mail or password.");
		}

		BCryptPasswordEncoder bCrypt = new BCryptPasswordEncoder();

		if (!bCrypt.matches(user.getPassword(), passwordRecoverd)) {
			throw new AuthorizationException("Invalid e-mail or password.");
		}

		saveCurrentLogin(user.getUsername());

		return jwtUtil.generateToken(user.getUsername());
	}

	@Transactional
	private void saveCurrentLogin(String username) {
		User user = findByLogin(username);

		user.setLastLogin(LocalDate.now());

		repository.save(user);
	}

	@Override
	public User findByLogin(String login) {

		User user = repository.findByLogin(login);

		if (user == null)
			throw new EntityNotFoundException(String.format("User login '%s' doesn't exist", login));

		return user;
	}

	@Override
	public void increaseCounter(User user, Integer counter) {

		if (counter > 0) {
			Integer userCounter = user.getCounter();
			if (userCounter >= 0) {
				user.setCounter(userCounter + counter);

				repository.save(user);
			}
		}
	}

	@Transactional
	@Override
	public void decreaseCounter(User user, Integer counter) {

		if (counter > 0) {
			Integer userCounter = user.getCounter();
			if (userCounter > 0) {
				user.setCounter(userCounter - counter);

				repository.save(user);
				repository.flush();
			}
		}
	}

	private void checkFields(User user) {
		if (StringUtils.isEmpty(user.getEmail()) || StringUtils.isEmpty(user.getFirstName())
				|| StringUtils.isEmpty(user.getLastName()) || StringUtils.isEmpty(user.getPassword())
				|| StringUtils.isEmpty(user.getLogin()) || StringUtils.isEmpty(user.getPhone())
				|| user.getBirthday() == null || user.getCars().isEmpty()) {
			throw new MissingFieldsException(Constants.MISSING_FIELDS);
		}
		for (Car car : user.getCars()) {
			if (StringUtils.isEmpty(car.getColor()) || StringUtils.isEmpty(car.getLicensePlate())
					|| StringUtils.isEmpty(car.getModel()) || car.getYearBuild() == null || car.getYearBuild() < 1) {
				throw new MissingFieldsException(Constants.MISSING_FIELDS);
			}

			carService.checkLicensePlate(car.getLicensePlate());
		}
	}

}
