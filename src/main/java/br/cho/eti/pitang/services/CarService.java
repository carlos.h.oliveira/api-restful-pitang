/**
 * 
 */
package br.cho.eti.pitang.services;

import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import br.cho.eti.pitang.domain.Car;
import br.cho.eti.pitang.repositories.CarRepository;

/**
 * @author Carlos H. de Oliveira - carlos.h.oliveira@cho.eti.br
 *
 */
public interface CarService {

	/**
	 * @see {@link CarRepository#findAllByUserLogin(String)}
	 */
	Set<Car> findAllByUserLogin(String userLogin);

	/**
	 * Save a new car
	 * 
	 * @param car
	 * @param userLogin
	 * @return an instance of Car
	 */
	Car save(Car car, String userLogin);

	/**
	 * Update a car
	 * 
	 * @param car
	 * @param id
	 * @param userLogin
	 * @return the car updated
	 */
	Car update(Car car, String id, String userLogin);

	/**
	 * Delete a car
	 * 
	 * @param id
	 * @param userLogin
	 */
	void delete(String id, String userLogin);

	/**
	 * Check if a car exists by id
	 * 
	 * @param id
	 */
	void exists(String id);

	/**
	 * Check if a car exists by license plate
	 * 
	 * @param licensePlate
	 */
	void existsByLicensePlate(String licensePlate);

	/**
	 * Check if a car exists by license plate and an user
	 * 
	 * @param licensePlate
	 * @param userLogin
	 */
	void existsByLicensePlateAndUserLoginNot(String licensePlate, String userLogin);

	/**
	 * Check if the car belongs to logged user
	 * 
	 * @param id
	 * @param userLogin
	 */
	void checkIfCarBelongsToUser(String id, String userLogin);

	/**
	 * Save an image of car
	 * 
	 * @param file
	 * @param id
	 * @param userLogin
	 * @return
	 */
	Car saveImage(MultipartFile file, String id, String userLogin);

	/**
	 * Delete all cars
	 * 
	 * @param cars
	 */
	void deleteAll(Iterable<Car> cars);

	/**
	 * Find a car by car id and user login
	 * 
	 * @param id
	 * @param userLogin
	 * @return
	 */
	Car findByIdAndUserLogin(String id, String userLogin);

	/**
	 * Check if a car exists by license plate
	 * 
	 * @param licensePlate
	 */
	void checkLicensePlate(String licensePlate);

}
