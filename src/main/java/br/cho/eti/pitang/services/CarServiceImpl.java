/**
 * 
 */
package br.cho.eti.pitang.services;

import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import br.cho.eti.pitang.domain.Car;
import br.cho.eti.pitang.domain.User;
import br.cho.eti.pitang.exceptions.BadRequestException;
import br.cho.eti.pitang.exceptions.EntityAlreadyExistsException;
import br.cho.eti.pitang.exceptions.EntityNotFoundException;
import br.cho.eti.pitang.exceptions.FileException;
import br.cho.eti.pitang.exceptions.MissingFieldsException;
import br.cho.eti.pitang.repositories.CarRepository;
import br.cho.eti.pitang.util.Constants;
import br.cho.eti.pitang.util.Util;

/**
 * @author Carlos H. de Oliveira - carlos.h.oliveira@cho.eti.br
 *
 */
@Service
class CarServiceImpl implements CarService {

	@Autowired
	private CarRepository repository;

	@Autowired
	private UserService userService;

	@Autowired
	private ImageService imageService;

	@Value("${img.prefix.car.profile}")
	private String prefix;

	@Value("${img.profile.size}")
	private Integer size;

	@Override
	public Set<Car> findAllByUserLogin(String userLogin) {

		return repository.findAllByUserLogin(userLogin);
	}

	@Override
	public Car findByIdAndUserLogin(String id, String userLogin) {

		Car car = repository.findByIdAndUserLogin(id, userLogin);

		if (car == null) {
			throw new EntityNotFoundException(String.format("There is not a car with id '%s' for logged user", id));
		}

		incrementCounter(car);

		userService.increaseCounter(car.getUser(), 1);

		return car;
	}

	@Transactional
	@Override
	public Car save(Car car, String userLogin) {

		if (!StringUtils.hasText(car.getId())) {
			car.setId(UUID.randomUUID().toString());
		}

		checkFields(car);

		String licensePlate = car.getLicensePlate();
		existsByLicensePlate(licensePlate);

		User user = userService.findByLogin(userLogin);

		car.setUser(user);

		return repository.save(car);
	}

	@Transactional
	@Override
	public Car update(Car car, String id, String userLogin) {

		checkFields(car);
		exists(id);

		Car carByPlate = repository.findByLicensePlateIgnoreCase(car.getLicensePlate());
		Car carById = repository.findById(id).get();

		if (carByPlate != null) {
			if (!carByPlate.getId().equalsIgnoreCase(carById.getId())) {
				throw new BadRequestException(String.format("The id '%s' and licence plate '%s' doesn't match",
						carById.getId(), car.getLicensePlate()));
			}
			if (!carByPlate.getUser().getLogin().equalsIgnoreCase(userLogin)) {
				throw new BadRequestException("This car and logged user doesn't match");
			}
		}
		userService.decreaseCounter(carById.getUser(), carById.getCounter());

		Util.copyNonNullProperties(car, carById);

		return repository.save(carById);
	}

	@Transactional
	@Override
	public void delete(String id, String userLogin) {

		exists(id);

		checkIfCarBelongsToUser(id, userLogin);

		repository.deleteById(id);
	}

	@Override
	public void checkIfCarBelongsToUser(String id, String userLogin) {

		userService.existsByLogin(userLogin);

		Car car = repository.findByIdAndUserLogin(id, userLogin);

		if (car == null || !car.getUser().getLogin().equalsIgnoreCase(userLogin)) {
			throw new BadRequestException("This car doesn't belong to this logged user");
		}
	}

	@Override
	public void exists(String id) {
		if (!repository.existsById(id)) {
			throw new EntityNotFoundException(String.format("Car id '%s' doens't exist", id));
		}
	}

	@Override
	public void existsByLicensePlate(String licensePlate) {

		if (repository.countByLicensePlateIgnoreCase(licensePlate) > 0) {
			throw new EntityAlreadyExistsException(String.format("License plate '%s' already exists", licensePlate));
		}
	}

	@Override
	public void existsByLicensePlateAndUserLoginNot(String licensePlate, String userLogin) {

		if (repository.countByLicensePlateIgnoreCaseAndUserLoginNot(licensePlate, userLogin) > 0) {
			throw new EntityAlreadyExistsException(String.format("License plate '%s' already exists", licensePlate));
		}
	}

	@Override
	public Car saveImage(MultipartFile file, String id, String userLogin) {
		checkIfCarBelongsToUser(id, userLogin);

		String originalFileName = StringUtils.cleanPath(file.getOriginalFilename());

		if (originalFileName.contains("..")) {
			throw new FileException("The file contains invalid characters");
		}

		try {
			Car car = repository.findById(id)
					.orElseThrow(() -> new EntityNotFoundException(String.format("Car id '%s' doens't exist", id)));

			BufferedImage jpgImage = imageService.getJpgImageFromFile(file);
			jpgImage = imageService.cropSquare(jpgImage);
			jpgImage = imageService.resize(jpgImage, size);

			String fileName = prefix + car.getId() + ".jpg";

			car.setFile(fileName);

			InputStream inputStream = imageService.getInputStream(jpgImage, "jpg");

			car.setImg(new byte[inputStream.available()]);

			return repository.save(car);
		} catch (Exception e) {
			throw new FileException(String.format("The file %s cannot be saved", originalFileName), e);
		}
	}

	@Override
	public void deleteAll(Iterable<Car> cars) {

		repository.deleteAll(cars);
	}

	private void checkFields(Car car) {
		if (StringUtils.isEmpty(car.getColor()) || StringUtils.isEmpty(car.getLicensePlate())
				|| StringUtils.isEmpty(car.getModel()) || car.getYearBuild() == null || car.getYearBuild() < 1) {
			throw new MissingFieldsException(Constants.MISSING_FIELDS);
		}

		checkLicensePlate(car.getLicensePlate());
	}

	@Override
	public void checkLicensePlate(String licensePlate) {

		Pattern pattern = Pattern.compile("[a-zA-Z]{3}-[a-zA-Z0-9]{4}");
		Matcher matcher = pattern.matcher(licensePlate);

		if (!matcher.matches()) {
			throw new BadRequestException(String.format("The license plate '%s' is invalid", licensePlate));
		}
	}

	@Transactional
	private void incrementCounter(Car car) {
		car.increment();

		repository.save(car);
	}

}
