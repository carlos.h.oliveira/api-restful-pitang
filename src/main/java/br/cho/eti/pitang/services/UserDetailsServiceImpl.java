package br.cho.eti.pitang.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import br.cho.eti.pitang.domain.User;
import br.cho.eti.pitang.exceptions.UsernameNotFoundException;
import br.cho.eti.pitang.repositories.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserRepository repository;

	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		User user = repository.findByLogin(login);

		if (user == null) {
			throw new UsernameNotFoundException("E-mail not found");
		}

		return user;
	}
}
