/**
 * 
 */
package br.cho.eti.pitang.dto;

import static br.cho.eti.pitang.util.Constants.INVALID_FIELDS;
import static br.cho.eti.pitang.util.Constants.MISSING_FIELDS;

import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.cho.eti.pitang.domain.Car;
import br.cho.eti.pitang.util.Util;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Carlos H. de Oliveira - carlos.h.oliveira@cho.eti.br
 *
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class CarPostDTO {

	@NotNull(message = INVALID_FIELDS)
	@Positive(message = INVALID_FIELDS)
	@JsonProperty("year")
	private Integer yearBuild;

	@NotBlank(message = MISSING_FIELDS)
	@NotNull(message = INVALID_FIELDS)
	private String licensePlate;

	@NotBlank(message = MISSING_FIELDS)
	@NotNull(message = INVALID_FIELDS)
	private String model;

	@NotBlank(message = MISSING_FIELDS)
	@NotNull(message = INVALID_FIELDS)
	private String color;

	public static Car convertToTO(CarPostDTO source) {
		Car target = new Car();

		Util.copyNonNullProperties(source, target);

		return target;
	}

	public static Set<Car> convertToTO(Set<CarPostDTO> cars) {
		return cars.stream().map(c -> convertToTO(c)).collect(Collectors.toSet());
	}

}
