/**
 * 
 */
package br.cho.eti.pitang.dto;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;

import br.cho.eti.pitang.domain.User;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Carlos H. de Oliveira - carlos.h.oliveira@cho.eti.br
 *
 */
@Getter
@Setter
@ToString
public class UserDTO {

	private String id;

	private String firstName;

	private String lastName;

	private String email;

	private LocalDate birthday;

	private String login;

	private LocalDate lastLogin;

	private LocalDate createdAt;

	private String phone;

	private String file;

	private byte[] img;

	private Set<CarDTO> cars;

	public static UserDTO convertToDTO(User source) {
		UserDTO target = new UserDTO();

		BeanUtils.copyProperties(source, target, "cars");

		Set<CarDTO> setCar = CarDTO.convertToDTO(source.getCars());

		target.setCars(setCar);

		return target;
	}

	public static List<UserDTO> convertToDTO(List<User> users) {
		return users.stream().map(UserDTO::convertToDTO).collect(Collectors.toList());
	}

}
