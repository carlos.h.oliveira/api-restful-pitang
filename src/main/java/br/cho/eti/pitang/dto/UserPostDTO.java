/**
 * 
 */
package br.cho.eti.pitang.dto;

import static br.cho.eti.pitang.util.Constants.INVALID_FIELDS;
import static br.cho.eti.pitang.util.Constants.MISSING_FIELDS;

import java.time.LocalDate;
import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.beans.BeanUtils;

import br.cho.eti.pitang.domain.User;
import br.cho.eti.pitang.exceptions.MissingFieldsException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Carlos H. de Oliveira - carlos.h.oliveira@cho.eti.br
 *
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class UserPostDTO {

	@NotBlank(message = INVALID_FIELDS)
	@NotNull(message = MISSING_FIELDS)
	private String firstName;

	@NotBlank(message = INVALID_FIELDS)
	@NotNull(message = MISSING_FIELDS)
	private String lastName;

	@NotNull(message = MISSING_FIELDS)
	@Email(message = INVALID_FIELDS)
	private String email;

	@NotNull(message = MISSING_FIELDS)
	private LocalDate birthday;

	@NotBlank(message = INVALID_FIELDS)
	@NotNull(message = MISSING_FIELDS)
	private String login;

	@NotBlank(message = INVALID_FIELDS)
	@NotNull(message = MISSING_FIELDS)
	private String password;

	@NotBlank(message = INVALID_FIELDS)
	@NotNull(message = MISSING_FIELDS)
	private String phone;

	private String file;

	@NotNull(message = MISSING_FIELDS)
	private Set<CarPostDTO> cars;

	public static User convertToTO(UserPostDTO source) {
		User target = new User();

		if (source.getCars() == null) {
			throw new MissingFieldsException(MISSING_FIELDS);
		}

		BeanUtils.copyProperties(source, target, "cars");

		target.setCars(CarPostDTO.convertToTO(source.getCars()));

		return target;
	}

}