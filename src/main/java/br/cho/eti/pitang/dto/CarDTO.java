/**
 * 
 */
package br.cho.eti.pitang.dto;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.cho.eti.pitang.domain.Car;
import br.cho.eti.pitang.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Carlos H. de Oliveira - carlos.h.oliveira@cho.eti.br
 *
 */
@Getter
@Setter
@ToString
public class CarDTO {

	private String id;

	@JsonProperty("year")
	private Integer yearBuild;

	private String licensePlate;

	private String model;

	private String color;

	private String file;

	private byte[] img;

	public static Car convertToTO(CarDTO source) {
		Car target = new Car();

		Util.copyNonNullProperties(source, target);

		return target;
	}

	public static Set<Car> convertToTO(Set<CarDTO> cars) {
		return cars.stream().map(c -> convertToTO(c)).collect(Collectors.toSet());
	}

	public static Set<CarDTO> convertToDTO(Set<Car> cars) {
		return cars.stream().map(c -> convertToDTO(c)).collect(Collectors.toSet());
	}

	public static CarDTO convertToDTO(Car source) {
		CarDTO target = new CarDTO();

		BeanUtils.copyProperties(source, target, "user");

		return target;
	}

}
