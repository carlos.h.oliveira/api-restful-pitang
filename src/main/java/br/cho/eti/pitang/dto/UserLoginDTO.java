/**
 * 
 */
package br.cho.eti.pitang.dto;

import javax.validation.constraints.NotBlank;

import br.cho.eti.pitang.domain.User;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Carlos H. de Oliveira - carlos.h.oliveira@cho.eti.br
 *
 */
@Getter
@Setter
@ToString
public class UserLoginDTO {

	@NotBlank
	private String login;

	@NotBlank
	private String password;

	public static User convertToTO(UserLoginDTO dto) {
		return User.builder().login(dto.getLogin()).password(dto.getPassword()).build();
	}

}
