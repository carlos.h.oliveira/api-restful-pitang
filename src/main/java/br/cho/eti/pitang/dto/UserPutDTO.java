/**
 * 
 */
package br.cho.eti.pitang.dto;

import static br.cho.eti.pitang.util.Constants.INVALID_FIELDS;
import static br.cho.eti.pitang.util.Constants.MISSING_FIELDS;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.beans.BeanUtils;

import br.cho.eti.pitang.domain.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Carlos H. de Oliveira - carlos.h.oliveira@cho.eti.br
 *
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class UserPutDTO {

	@NotEmpty(message = INVALID_FIELDS)
	@NotNull(message = MISSING_FIELDS)
	private String firstName;

	@NotEmpty(message = INVALID_FIELDS)
	@NotNull(message = MISSING_FIELDS)
	private String lastName;

	@NotNull(message = MISSING_FIELDS)
	@Email(message = INVALID_FIELDS)
	private String email;

	@NotNull(message = MISSING_FIELDS)
	private LocalDate birthday;

	@NotEmpty(message = INVALID_FIELDS)
	@NotNull(message = MISSING_FIELDS)
	private String login;

	@NotEmpty(message = INVALID_FIELDS)
	@NotNull(message = MISSING_FIELDS)
	private String password;

	@NotEmpty(message = INVALID_FIELDS)
	@NotNull(message = MISSING_FIELDS)
	private String phone;

	private String file;

	public static User convertToTO(UserPutDTO source) {
		User target = new User();

		BeanUtils.copyProperties(source, target, "cars");

		return target;
	}

}
