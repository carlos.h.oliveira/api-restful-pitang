package br.cho.eti.pitang.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import br.cho.eti.pitang.domain.User;
import br.cho.eti.pitang.dto.UserDTO;
import br.cho.eti.pitang.dto.UserPostDTO;
import br.cho.eti.pitang.dto.UserPutDTO;
import br.cho.eti.pitang.services.UserService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Carlos H. de Oliveira - carlos.h.oliveira@cho.eti.br
 */
@RestController
@RequestMapping("users")
public class UserResource {

	@Autowired
	private UserService service;

	@ApiResponses(value = { @ApiResponse(code = 200, message = "A list with user DTOs") })
	@GetMapping
	public ResponseEntity<List<UserDTO>> findAll() {
		return ResponseEntity.ok(UserDTO.convertToDTO(service.findAll()));
	}

	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "DTO with user information"), //
			@ApiResponse(code = 404, message = "User doens't exist"), //
	})
	@GetMapping(path = "/{id}")
	public ResponseEntity<UserDTO> getById(@PathVariable("id") String id) {
		return ResponseEntity.ok(UserDTO.convertToDTO(service.findById(id)));
	}

	@ApiResponses(value = { //
			@ApiResponse(code = 201, message = "The id of new user"), //
			@ApiResponse(code = 409, message = "Email already exists OR Login already exists OR License plate already exists"), //
			@ApiResponse(code = 400, message = "Missing Fields OR Invalid Fields"), //
	})
	@PostMapping
	public ResponseEntity<String> save(@Valid @RequestBody UserPostDTO dto) {
		User userSaved = service.save(UserPostDTO.convertToTO(dto));
		return ResponseEntity.created(UriComponentsBuilder.fromPath("/{id}").buildAndExpand(userSaved.getId()).toUri())
				.build();
	}

	@ApiResponses(value = { //
			@ApiResponse(code = 204, message = "No content"), //
			@ApiResponse(code = 404, message = "User doens't exist"), //
	})
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<String> delete(@PathVariable("id") String id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}

	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "The id of new user"), //
			@ApiResponse(code = 409, message = "Email already exists OR Login already exists"), //
			@ApiResponse(code = 400, message = "Missing Fields OR Invalid Fields"), //
			@ApiResponse(code = 404, message = "User doens't exist"), //
	})
	@PutMapping(path = "/{id}")
	public ResponseEntity<UserDTO> update(@PathVariable("id") String id, @Valid @RequestBody UserPutDTO dto) {
		User userUpdated = service.update(UserPutDTO.convertToTO(dto), id);
		return ResponseEntity.ok(UserDTO.convertToDTO(userUpdated));
	}

	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "DTO with user information"), //
			@ApiResponse(code = 404, message = "User doens't exist"), //
	})
	@PostMapping("/upload/{id}")
	public ResponseEntity<UserDTO> upload(@RequestParam("file") MultipartFile file, @PathVariable("id") String id) {
		User user = service.saveImage(file, id);

		return ResponseEntity.ok(UserDTO.convertToDTO(user));
	}

}