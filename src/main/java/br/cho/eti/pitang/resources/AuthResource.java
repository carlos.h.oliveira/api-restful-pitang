package br.cho.eti.pitang.resources;

import java.security.Principal;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.cho.eti.pitang.dto.UserDTO;
import br.cho.eti.pitang.dto.UserLoginDTO;
import br.cho.eti.pitang.services.UserService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Carlos H. de Oliveira - carlos.h.oliveira@cho.eti.br
 */
@RestController
@RequestMapping
public class AuthResource {

	@Autowired
	private UserService service;

	@ApiResponses(value = { @ApiResponse(code = 200, message = "User token"),
			@ApiResponse(code = 401, message = "Invalid login or password"), })
	@PostMapping(path = "/signin")
	public ResponseEntity<String> signin(@Valid @RequestBody UserLoginDTO dto, HttpServletResponse response) {
		String token = service.signin(UserLoginDTO.convertToTO(dto));

		String bearerToken = "Bearer " + token;
		response.addHeader("Authorization", bearerToken);
		response.addHeader("access-control-expose-headers", "Authorization");

		return ResponseEntity.ok(bearerToken);
	}

	@ApiResponses(value = { @ApiResponse(code = 200, message = "DTO with user information"),
			@ApiResponse(code = 401, message = "Unauthorized - invalid session OR Unauthorized"), })
	@GetMapping(path = "/me")
	public ResponseEntity<UserDTO> me(Principal principal) {
		return ResponseEntity.ok(UserDTO.convertToDTO(service.findByLogin(principal.getName())));
	}

}