package br.cho.eti.pitang.resources;

import java.security.Principal;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import br.cho.eti.pitang.domain.Car;
import br.cho.eti.pitang.dto.CarDTO;
import br.cho.eti.pitang.dto.CarPostDTO;
import br.cho.eti.pitang.dto.CarPutDTO;
import br.cho.eti.pitang.services.CarService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Carlos H. de Oliveira - carlos.h.oliveira@cho.eti.br
 */
@RestController
@RequestMapping("cars")
public class CarResource {

	@Autowired
	private CarService service;

	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "A car DTO of logged user"), //
	})
	@GetMapping
	public ResponseEntity<Set<CarDTO>> findAll(Principal principal) {
		Set<Car> cars = service.findAllByUserLogin(principal.getName());

		return ResponseEntity.ok(CarDTO.convertToDTO(cars));
	}

	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "The DTO of car of the logged user"), //
			@ApiResponse(code = 400, message = "The car and user doens't match"), //
	})
	@GetMapping(path = "/{id}")
	public ResponseEntity<CarDTO> getById(@PathVariable("id") String id, Principal principal) {
		Car car = service.findByIdAndUserLogin(id, principal.getName());

		return ResponseEntity.ok(CarDTO.convertToDTO(car));
	}

	@ApiResponses(value = { //
			@ApiResponse(code = 201, message = "The DTO of car of the logged user"), //
			@ApiResponse(code = 409, message = "License already exists"), //
			@ApiResponse(code = 400, message = "Missing Fields OR Invalid Fields OR The car and user doens't match"), //
	})
	@PostMapping
	public ResponseEntity<String> save(@Valid @RequestBody CarPostDTO dto, Principal principal) {
		Car carSaved = service.save(CarPostDTO.convertToTO(dto), principal.getName());

		return ResponseEntity.created(UriComponentsBuilder.fromPath("/{id}").buildAndExpand(carSaved.getId()).toUri())
				.build();
	}

	@ApiResponses(value = { //
			@ApiResponse(code = 204, message = "No content"), //
			@ApiResponse(code = 404, message = "Car doens't exist"), //
			@ApiResponse(code = 400, message = "Missing Fields OR Invalid Fields OR The car and user doens't match"), //
	})
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<String> delete(@PathVariable("id") String id, Principal principal) {
		service.delete(id, principal.getName());

		return ResponseEntity.noContent().build();
	}

	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "DTO with car information of the logged user"), //
			@ApiResponse(code = 409, message = "License already exists"), //
			@ApiResponse(code = 400, message = "Missing Fields OR Invalid Fields OR The car and user doens't match"), //
			@ApiResponse(code = 404, message = "Car doens't exist"), //
	})
	@PutMapping(path = "/{id}")
	public ResponseEntity<CarDTO> update(@PathVariable("id") String id, @Valid @RequestBody CarPutDTO dto,
			Principal principal) {
		Car carUpdated = service.update(CarPutDTO.convertToTO(dto), id, principal.getName());

		return ResponseEntity.ok(CarDTO.convertToDTO(carUpdated));
	}

	@ApiResponses(value = { //
			@ApiResponse(code = 200, message = "DTO with car information of the logged user"), //
			@ApiResponse(code = 404, message = "Car doens't exist"), //
			@ApiResponse(code = 400, message = "Missing Fields OR Invalid Fields OR The car and user doens't match"), //
	})
	@PostMapping("/upload/{id}")
	public ResponseEntity<CarDTO> upload(@RequestParam("file") MultipartFile file, @PathVariable("id") String id,
			Principal principal) {
		Car car = service.saveImage(file, id, principal.getName());

		return ResponseEntity.ok(CarDTO.convertToDTO(car));
	}
}