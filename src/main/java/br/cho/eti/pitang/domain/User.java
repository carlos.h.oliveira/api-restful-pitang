/**
 * 
 */
package br.cho.eti.pitang.domain;

import static br.cho.eti.pitang.util.Constants.INVALID_FIELDS;
import static br.cho.eti.pitang.util.Constants.MISSING_FIELDS;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Carlos H. de Oliveira - carlos.h.oliveira@cho.eti.br
 */
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "tb_user")
@EntityListeners(AuditingEntityListener.class)
public class User extends BaseEntity implements UserDetails {

	private static final long serialVersionUID = 1L;

	@NotBlank(message = INVALID_FIELDS)
	@NotNull(message = MISSING_FIELDS)
	@Column(nullable = false)
	private String firstName;

	@NotBlank(message = INVALID_FIELDS)
	@NotNull(message = MISSING_FIELDS)
	@Column(nullable = false)
	private String lastName;

	@NotBlank(message = INVALID_FIELDS)
	@NotNull(message = MISSING_FIELDS)
	@Email(message = INVALID_FIELDS)
	@Column(nullable = false)
	private String email;

	@NotNull(message = MISSING_FIELDS)
	@Column(nullable = false)
	private LocalDate birthday;

	@NotBlank(message = INVALID_FIELDS)
	@NotNull(message = MISSING_FIELDS)
	@Column(nullable = false)
	private String login;

	@NotBlank(message = INVALID_FIELDS)
	@NotNull(message = MISSING_FIELDS)
	@Column(nullable = false)
	private String password;

	@NotBlank(message = INVALID_FIELDS)
	@NotNull(message = MISSING_FIELDS)
	@Column(nullable = false)
	private String phone;

	@CreatedDate
	@Column(nullable = false, updatable = false)
	private LocalDate createdAt;

	@Column(nullable = true)
	private LocalDate lastLogin;

	@Column(nullable = true)
	private String file;

	@Builder.Default
	@Column
	private Integer counter = 0;

	@Lob
	private byte[] img;

	@OrderBy("counter DESC, model ASC")
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Car> cars;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getUsername() {
		return login;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public User(String id) {
		this.id = id;
	}

}
