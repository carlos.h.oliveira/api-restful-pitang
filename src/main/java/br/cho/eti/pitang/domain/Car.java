/**
 * 
 */
package br.cho.eti.pitang.domain;

import static br.cho.eti.pitang.util.Constants.INVALID_FIELDS;
import static br.cho.eti.pitang.util.Constants.MISSING_FIELDS;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Carlos H. de Oliveira - carlos.h.oliveira@cho.eti.br
 */
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(value = { "user" }, allowGetters = false)
@ToString
@Entity
@Table(name = "tb_car")
public class Car extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@NotNull(message = MISSING_FIELDS)
	@Positive(message = INVALID_FIELDS)
	private Integer yearBuild;

	@NotBlank(message = INVALID_FIELDS)
	@NotNull(message = MISSING_FIELDS)
	private String licensePlate;

	@NotBlank(message = INVALID_FIELDS)
	@NotNull(message = MISSING_FIELDS)
	private String model;

	@NotBlank(message = INVALID_FIELDS)
	@NotNull(message = MISSING_FIELDS)
	private String color;

	@ManyToOne(cascade = CascadeType.MERGE)
	private User user;

	@Builder.Default
	private Integer counter = 0;

	private String file;

	@Lob
	private byte[] img;

	public void increment() {
		counter++;
	}
}
