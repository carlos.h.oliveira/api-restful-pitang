/**
 * 
 */
package br.cho.eti.pitang.domain;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Carlos H. de Oliveira - carlos.h.oliveira@cho.eti.br
 *
 */
@Getter
@Setter
@MappedSuperclass
abstract class BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	public String id;

}
