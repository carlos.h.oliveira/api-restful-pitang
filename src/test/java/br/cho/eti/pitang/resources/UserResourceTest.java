package br.cho.eti.pitang.resources;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import java.time.LocalDate;
import java.util.Collections;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import br.cho.eti.pitang.domain.Car;
import br.cho.eti.pitang.domain.User;
import br.cho.eti.pitang.dto.UserPostDTO;
import br.cho.eti.pitang.repositories.UserRepository;
import br.cho.eti.pitang.util.Factory;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UserResourceTest {

        @Autowired
        private MockMvc mockMvc;

        @Autowired
        private UserRepository userRepository;

        private User user;

        @BeforeEach
        protected void setUp() {
                this.user = Factory.getUser();

                userRepository.saveAndFlush(this.user);
        }

        @AfterEach
        protected void tearDown() {
                userRepository.deleteAll();
        }

        @Test
        public void shouldAllowDeleteUser() throws Exception {
                mockMvc.perform(delete("/users/{id}", this.user.getId()))
                                .andExpect(status().isNoContent());
        }

        @Test
        public void shouldNotDeleteANonExistentUser() throws Exception {
                mockMvc.perform(delete("/users/{id}", "userId"))
                                .andExpect(status().isNotFound());
        }

        @Test
        public void shouldReturnTheUserById() throws Exception {
                // Suponha que o serviço UserService retorne um usuário quando o ID existir
                String userId = "123"; // ID de usuário existente

                // Crie um objeto User fictício com os campos especificados e o Car associado
                User user = User.builder()
                                .birthday(LocalDate.of(1984, 4, 4))
                                .email("testuser@gmail.com")
                                .firstName("Test")
                                .lastName("User")
                                .login("testuser@gmail.com")
                                .password("123456")
                                .phone("81998892223")
                                .build();

                // Crie um objeto Car fictício associado ao User
                Car car = Car.builder()
                                .color("White")
                                .licensePlate("PPP-0123")
                                .model("Renault")
                                .yearBuild(2018)
                                .build();
                car.setId("456");
                car.setUser(user);

                user.setId(userId);
                user.setCars(Collections.singleton(car)); // Associe o Car ao User

                userRepository.saveAndFlush(user);

                mockMvc.perform(get("/users/{id}", userId))
                                .andExpect(status().isOk()) // Verifica se a resposta tem status HTTP 200 (OK)
                                .andExpect(jsonPath("$.id").value(userId)) // Verifica se o JSON de resposta contém o ID
                                                                           // correto
                                .andExpect(jsonPath("$.firstName").value(user.getFirstName())) // Verifica o atributo
                                                                                               // "firstName"
                                .andExpect(jsonPath("$.lastName").value(user.getLastName())) // Verifica o atributo
                                                                                             // "lastName"
                                .andExpect(jsonPath("$.email").value(user.getEmail())) // Verifica o atributo "email"
                                .andExpect(jsonPath("$.birthday").value(user.getBirthday().toString())) // Verifica o
                                                                                                        // atributo
                                                                                                        // "birthday"
                                .andExpect(jsonPath("$.login").value(user.getLogin())) // Verifica o atributo "login"
                                .andExpect(jsonPath("$.phone").value(user.getPhone())) // Verifica o atributo "phone"
                                .andExpect(jsonPath("$.cars[0].color").value(car.getColor())) // Verifica o atributo
                                                                                              // "color" do Car
                                .andExpect(jsonPath("$.cars[0].licensePlate").value(car.getLicensePlate())) // Verifica
                                                                                                            // o
                                                                                                            // atributo
                                                                                                            // "licensePlate"
                                                                                                            // do Car
                                .andExpect(jsonPath("$.cars[0].model").value(car.getModel())) // Verifica o atributo
                                                                                              // "model" do Car
                                .andExpect(jsonPath("$.cars[0].year").value(car.getYearBuild())); // Verifica o atributo
                                                                                                  // "yearBuild" do Car
        }

        @Test
        public void shouldReturnNotFound() throws Exception {
                // Suponha que o serviço UserService retorne um usuário quando o ID existir
                String userId = "000"; // ID de usuário existente

                mockMvc.perform(get("/users/{id}", userId))
                                .andExpect(status().isNotFound());
        }

        @Test
        public void shouldConflictWhenCreating() throws Exception {
                // Suponha que a criação do usuário seja bem-sucedida
                UserPostDTO validUserDto = Factory.createValidUserPostDTO();

                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.registerModule(new JavaTimeModule());

                // Envia uma solicitação POST com o DTO válido
                MockHttpServletRequestBuilder request = post("/users")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(validUserDto));

                mockMvc.perform(request)
                                .andExpect(status().isConflict()); // Verifica se a resposta tem status HTTP 201
                                                                   // (Created)
        }

        @Test
        public void shouldSave() throws Exception {
                userRepository.deleteAll();

                // Suponha que a criação do usuário seja bem-sucedida
                UserPostDTO validUserDto = Factory.createValidUserPostDTO();

                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.registerModule(new JavaTimeModule());

                // Envia uma solicitação POST com o DTO válido
                MockHttpServletRequestBuilder request = post("/users")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(validUserDto));

                mockMvc.perform(request)
                                .andExpect(status().isCreated()); // Verifica se a resposta tem status HTTP 201
                                                                  // (Created)
        }

        @Test
        public void sholudConflictWhenFirstNameIsNull() throws Exception {
                // Suponha que a criação do usuário seja bem-sucedida
                UserPostDTO validUserDto = Factory.createValidUserPostDTO();
                validUserDto.setFirstName(null);

                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.registerModule(new JavaTimeModule());

                // Envia uma solicitação POST com o DTO válido
                MockHttpServletRequestBuilder request = post("/users")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(validUserDto));

                mockMvc.perform(request)
                                .andExpect(status().isBadRequest()); // Verifica se a resposta tem status HTTP 201
                                                                     // (Created)
        }

        @Test
        public void shouldReturnBadRequestWhenLastNameIsNull() throws Exception {
                UserPostDTO userDto = Factory.createValidUserPostDTO();
                userDto.setLastName(null);

                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.registerModule(new JavaTimeModule());

                MockHttpServletRequestBuilder request = post("/users")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(userDto));

                mockMvc.perform(request)
                                .andExpect(status().isBadRequest());
        }

        @Test
        public void shouldReturnBadRequestWhenEmailIsNull() throws Exception {
                UserPostDTO userDto = Factory.createValidUserPostDTO();
                userDto.setEmail(null);

                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.registerModule(new JavaTimeModule());

                MockHttpServletRequestBuilder request = post("/users")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(userDto));

                mockMvc.perform(request)
                                .andExpect(status().isBadRequest());
        }

        @Test
        public void shouldReturnBadRequestWhenBirthdayIsNull() throws Exception {
                UserPostDTO userDto = Factory.createValidUserPostDTO();
                userDto.setBirthday(null);

                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.registerModule(new JavaTimeModule());

                MockHttpServletRequestBuilder request = post("/users")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(userDto));

                mockMvc.perform(request)
                                .andExpect(status().isBadRequest());
        }

        @Test
        public void shouldReturnBadRequestWhenLoginIsNull() throws Exception {
                UserPostDTO userDto = Factory.createValidUserPostDTO();
                userDto.setLogin(null);

                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.registerModule(new JavaTimeModule());

                MockHttpServletRequestBuilder request = post("/users")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(userDto));

                mockMvc.perform(request)
                                .andExpect(status().isBadRequest());
        }

        @Test
        public void shouldReturnBadRequestWhenPasswordIsNull() throws Exception {
                UserPostDTO userDto = Factory.createValidUserPostDTO();
                userDto.setPassword(null);

                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.registerModule(new JavaTimeModule());

                MockHttpServletRequestBuilder request = post("/users")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(userDto));

                mockMvc.perform(request)
                                .andExpect(status().isBadRequest());
        }

        @Test
        public void shouldReturnBadRequestWhenPhoneIsNull() throws Exception {
                UserPostDTO userDto = Factory.createValidUserPostDTO();
                userDto.setPhone(null);

                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.registerModule(new JavaTimeModule());

                MockHttpServletRequestBuilder request = post("/users")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(userDto));

                mockMvc.perform(request)
                                .andExpect(status().isBadRequest());
        }

        @Test
        public void sholudConflictWhenIsNull() throws Exception {
                // Suponha que a criação do usuário seja bem-sucedida
                UserPostDTO validUserDto = Factory.createValidUserPostDTO();
                validUserDto.setCars(null);

                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.registerModule(new JavaTimeModule());

                // Envia uma solicitação POST com o DTO válido
                MockHttpServletRequestBuilder request = post("/users")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(validUserDto));

                mockMvc.perform(request)
                                .andExpect(status().isBadRequest());
        }

}
