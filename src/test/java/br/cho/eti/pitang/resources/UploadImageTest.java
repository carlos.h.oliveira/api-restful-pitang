package br.cho.eti.pitang.resources;

import java.nio.file.Path;
import java.time.LocalDate;
import java.util.Collections;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import br.cho.eti.pitang.domain.Car;
import br.cho.eti.pitang.domain.User;
import br.cho.eti.pitang.repositories.CarRepository;
import br.cho.eti.pitang.repositories.UserRepository;
import br.cho.eti.pitang.services.ImageService;

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(value = "testuser@gmail.com")
@ActiveProfiles("test")
@Transactional // Adicione esta anotação para rolar de volta as transações após cada teste
public class UploadImageTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private UserRepository userRepository;

    @Mock
    private ImageService imageService; // Mock do seu ImageService

    // Limpa o banco de dados antes de cada teste
    @BeforeEach
    public void setUp() {
        carRepository.deleteAll();
        userRepository.deleteAll();
    }

    // Limpa o banco de dados após cada teste
    @AfterEach
    public void tearDown() {
        carRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void testUploadCarImageSuccess() throws Exception {
        // Crie um carro fictício e salve-o no banco de dados em memória
        createUserAndCar();

        // Localização do arquivo de imagem real
        String filePath = "src/test/resources/kwid.jpg";

        // Carrega o arquivo de imagem real como um recurso
        Resource imageResource = new PathResource(Path.of(filePath));

        // Converte o recurso em um MultipartFile
        MockMultipartFile multipartFile = new MockMultipartFile("file", "kwid.jpg",
                MediaType.IMAGE_JPEG_VALUE, imageResource.getInputStream());

        // Envie uma solicitação POST para o endpoint de upload com o ID do carro
        mockMvc.perform(MockMvcRequestBuilders.multipart("/cars/upload/{id}", "456")
                .file(multipartFile)
                .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.file").exists());
    }

    @Test
    public void testUploadUserImageSuccess() throws Exception {
        // Crie um carro fictício e salve-o no banco de dados em memória
        createUserAndCar();

        // Localização do arquivo de imagem real
        String filePath = "src/test/resources/user.png";

        // Carrega o arquivo de imagem real como um recurso
        Resource imageResource = new PathResource(Path.of(filePath));

        // Converte o recurso em um MultipartFile
        MockMultipartFile multipartFile = new MockMultipartFile("file", "user.png",
                MediaType.IMAGE_JPEG_VALUE, imageResource.getInputStream());

        // Envie uma solicitação POST para o endpoint de upload com o ID do carro
        mockMvc.perform(MockMvcRequestBuilders.multipart("/users/upload/{id}", "123")
                .file(multipartFile)
                .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.file").exists());
    }

    private void createUserAndCar() {
        userRepository.deleteAll();
        carRepository.deleteAll();

        // Suponha que o serviço UserService retorne um usuário quando o ID existir
        String userId = "123"; // ID de usuário existente

        // Crie um objeto User fictício com os campos especificados e o Car associado
        User user = User.builder()
                .birthday(LocalDate.of(1984, 4, 4))
                .email("testuser@gmail.com")
                .firstName("Test")
                .lastName("User")
                .login("testuser@gmail.com")
                .password("123456")
                .phone("81998892223")
                .build();

        // Crie um objeto Car fictício associado ao User
        Car car = Car.builder()
                .color("White")
                .licensePlate("PPP-0123")
                .model("Renault")
                .yearBuild(2018)
                .build();
        String carId = "456";
        car.setId(carId);
        car.setUser(user);

        user.setId(userId);
        user.setCars(Collections.singleton(car)); // Associe o Car ao User

        userRepository.saveAndFlush(user);
    }

}
