package br.cho.eti.pitang.resources;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import java.time.LocalDate;
import java.util.Collections;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import br.cho.eti.pitang.domain.Car;
import br.cho.eti.pitang.domain.User;
import br.cho.eti.pitang.dto.CarPostDTO;
import br.cho.eti.pitang.dto.UserPostDTO;
import br.cho.eti.pitang.repositories.CarRepository;
import br.cho.eti.pitang.repositories.UserRepository;
import br.cho.eti.pitang.security.JWTUtil;
import br.cho.eti.pitang.util.Factory;

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(value = "testuser@gmail.com")
@ActiveProfiles("test")
public class CarResourceTest {

        @Autowired
        private MockMvc mockMvc;

        @Autowired
        private UserRepository userRepository;

        @Autowired
        private CarRepository carRepository;

        @Autowired
        private JWTUtil jwtUtil;

        protected String token;

        private User user;

        private Car car;

        @BeforeEach
        protected void setUp() {
                this.user = Factory.getUser();
                this.car = this.user.getCars().iterator().next();

                userRepository.saveAndFlush(this.user);

                this.token = jwtUtil.generateToken("testuser@gmail.com");
        }

        @AfterEach
        protected void tearDown() {
                userRepository.deleteAll();
        }

        @Test
        public void shouldAllowDeleteCar() throws Exception {
                mockMvc.perform(delete("/cars/{id}", this.car.getId())
                                .header("Authorization", "Bearer " + this.token))
                                .andExpect(status().isNoContent());
        }

        @Test
        public void shouldNotDeleteANonExistentUser() throws Exception {
                mockMvc.perform(delete("/cars/{id}", "carId")
                                .header("Authorization", "Bearer " + this.token))
                                .andExpect(status().isNotFound());
        }

        @Test
        public void shouldReturnTheCarById() throws Exception {
                userRepository.deleteAll();
                carRepository.deleteAll();

                // Suponha que o serviço UserService retorne um usuário quando o ID existir
                String userId = "908"; // ID de usuário existente

                // Crie um objeto User fictício com os campos especificados e o Car associado
                User user = User.builder()
                                .birthday(LocalDate.of(1984, 4, 4))
                                .email("testuser@gmail.com")
                                .firstName("Test")
                                .lastName("User")
                                .login("testuser@gmail.com")
                                .password("123456")
                                .phone("81998892223")
                                .build();

                // Crie um objeto Car fictício associado ao User
                Car car = Car.builder()
                                .color("White")
                                .licensePlate("PPP-0123")
                                .model("Renault")
                                .yearBuild(2018)
                                .build();
                String carId = "444";
                car.setId(carId);
                car.setUser(user);

                user.setId(userId);
                user.setCars(Collections.singleton(car)); // Associe o Car ao User

                userRepository.saveAndFlush(user);

                mockMvc.perform(get("/cars/{id}", carId)
                                .header("Authorization", "Bearer " + this.token))
                                .andExpect(status().isOk()) // Verifica se a resposta tem status HTTP 200 (OK)
                                .andExpect(jsonPath("$.id").value(carId)) // Verifica se o JSON de resposta contém o ID
                                .andExpect(jsonPath("$.color").value(car.getColor())) // Verifica o atributo
                                .andExpect(jsonPath("$.licensePlate").value(car.getLicensePlate())) // Verifica
                                .andExpect(jsonPath("$.model").value(car.getModel())) // Verifica o atributo
                                .andExpect(jsonPath("$.year").value(car.getYearBuild())); // Verifica o atributo
        }

        @Test
        public void shouldReturnNotFound() throws Exception {
                mockMvc.perform(get("/cars/{id}", "000")
                                .header("Authorization", "Bearer " + this.token))
                                .andExpect(status().isNotFound());
        }

        @Test
        public void shouldConflictWhenCreating() throws Exception {
                // Suponha que a criação do usuário seja bem-sucedida
                UserPostDTO validUserDto = Factory.createValidUserPostDTO();

                CarPostDTO carPost = validUserDto.getCars().iterator().next();

                ObjectMapper objectMapper = new ObjectMapper();

                // Envia uma solicitação POST com o DTO válido
                MockHttpServletRequestBuilder request = post("/cars")
                                .header("Authorization", "Bearer " + this.token)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(carPost));

                mockMvc.perform(request)
                                .andExpect(status().isConflict());
        }

        @Test
        public void shouldSave() throws Exception {
                // Suponha que a criação do usuário seja bem-sucedida
                UserPostDTO validUserDto = Factory.createValidUserPostDTO();
                CarPostDTO carPost = validUserDto.getCars().iterator().next();
                carPost.setLicensePlate("XXX-1E84");

                ObjectMapper objectMapper = new ObjectMapper();

                // Envia uma solicitação POST com o DTO válido
                MockHttpServletRequestBuilder request = post("/cars")
                                .contentType(MediaType.APPLICATION_JSON)
                                .header("Authorization", "Bearer " + this.token)
                                .content(objectMapper.writeValueAsString(carPost));

                mockMvc.perform(request)
                                .andExpect(status().isCreated());
        }

        @Test
        public void sholudConflictWhenColorIsNull() throws Exception {
                // Suponha que a criação do usuário seja bem-sucedida
                UserPostDTO validUserDto = Factory.createValidUserPostDTO();
                CarPostDTO carPost = validUserDto.getCars().iterator().next();
                carPost.setColor(null);

                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.registerModule(new JavaTimeModule());

                // Envia uma solicitação POST com o DTO válido
                MockHttpServletRequestBuilder request = post("/cars")
                                .contentType(MediaType.APPLICATION_JSON)
                                .header("Authorization", "Bearer " + this.token)
                                .content(objectMapper.writeValueAsString(validUserDto));

                mockMvc.perform(request)
                                .andExpect(status().isBadRequest()); // Verifica se a resposta tem status HTTP 201
                                                                     // (Created)
        }

        @Test
        public void shouldReturnBadRequestWhenLicensePlateIsNull() throws Exception {
                UserPostDTO userDto = Factory.createValidUserPostDTO();
                CarPostDTO carPost = userDto.getCars().iterator().next();
                carPost.setLicensePlate(null);

                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.registerModule(new JavaTimeModule());

                MockHttpServletRequestBuilder request = post("/cars")
                                .contentType(MediaType.APPLICATION_JSON)
                                .header("Authorization", "Bearer " + this.token)
                                .content(objectMapper.writeValueAsString(userDto));

                mockMvc.perform(request)
                                .andExpect(status().isBadRequest());
        }

        @Test
        public void shouldReturnBadRequestWhenModelIsNull() throws Exception {
                UserPostDTO userDto = Factory.createValidUserPostDTO();
                CarPostDTO carPost = userDto.getCars().iterator().next();
                carPost.setModel(null);

                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.registerModule(new JavaTimeModule());

                MockHttpServletRequestBuilder request = post("/cars")
                                .contentType(MediaType.APPLICATION_JSON)
                                .header("Authorization", "Bearer " + this.token)
                                .content(objectMapper.writeValueAsString(userDto));

                mockMvc.perform(request)
                                .andExpect(status().isBadRequest());
        }

        @Test
        public void shouldReturnBadRequestWhenYearIsNull() throws Exception {
                UserPostDTO userDto = Factory.createValidUserPostDTO();
                CarPostDTO carPost = userDto.getCars().iterator().next();
                carPost.setYearBuild(null);

                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.registerModule(new JavaTimeModule());

                MockHttpServletRequestBuilder request = post("/cars")
                                .contentType(MediaType.APPLICATION_JSON)
                                .header("Authorization", "Bearer " + this.token)
                                .content(objectMapper.writeValueAsString(userDto));

                mockMvc.perform(request)
                                .andExpect(status().isBadRequest());
        }

        @Test
        public void shouldUpdate() throws Exception {
                userRepository.deleteAll();
                carRepository.deleteAll();

                // Suponha que o serviço UserService retorne um usuário quando o ID existir
                String userId = "908"; // ID de usuário existente

                // Crie um objeto User fictício com os campos especificados e o Car associado
                User user = User.builder()
                                .birthday(LocalDate.of(1984, 4, 4))
                                .email("testuser@gmail.com")
                                .firstName("Test")
                                .lastName("User")
                                .login("testuser@gmail.com")
                                .password("123456")
                                .phone("81998892223")
                                .build();

                // Crie um objeto Car fictício associado ao User
                Car car = Car.builder()
                                .color("White")
                                .licensePlate("PPP-0123")
                                .model("Renault")
                                .yearBuild(2018)
                                .build();
                String carId = "444";
                car.setId(carId);
                car.setUser(user);

                user.setId(userId);
                user.setCars(Collections.singleton(car)); // Associe o Car ao User

                userRepository.saveAndFlush(user);

                // Suponha que a criação do usuário seja bem-sucedida
                UserPostDTO validUserDto = Factory.getUserPostDTOFromCreatedUser(user);
                CarPostDTO carPost = validUserDto.getCars().iterator().next();
                carPost.setLicensePlate("XZX-1E84");
                carPost.setColor("Black");
                carPost.setModel("Fiat");

                ObjectMapper objectMapper = new ObjectMapper();

                // Envia uma solicitação POST com o DTO válido
                MockHttpServletRequestBuilder request = put("/cars/{id}", carId)
                                .contentType(MediaType.APPLICATION_JSON)
                                .header("Authorization", "Bearer " + this.token)
                                .content(objectMapper.writeValueAsString(carPost));

                mockMvc.perform(request)
                                .andExpect(status().isOk());
        }

}
