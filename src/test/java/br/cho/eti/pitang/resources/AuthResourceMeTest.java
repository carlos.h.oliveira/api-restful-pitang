package br.cho.eti.pitang.resources;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import br.cho.eti.pitang.domain.User;
import br.cho.eti.pitang.repositories.UserRepository;
import br.cho.eti.pitang.security.JWTUtil;
import br.cho.eti.pitang.util.Factory;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@WithMockUser(value = "testuser@gmail.com")
@SpringBootTest
@ActiveProfiles("test")
public class AuthResourceMeTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JWTUtil jwtUtil;

    protected String token;

    private User user;

    @BeforeEach
    protected void setUp() {
        this.user = Factory.getUser();

        userRepository.saveAndFlush(this.user);

        this.token = jwtUtil.generateToken("testuser@gmail.com");
    }

    @AfterEach
    protected void tearDown() {
        userRepository.deleteAll();
    }

    @Test
    void shouldAuthenticateUser() throws Exception {

        mockMvc
                .perform(
                        MockMvcRequestBuilders.get("/me")
                                .header("Authorization", "Bearer " + this.token)
                                .with(
                                        request -> {
                                            request.setRemoteUser(this.user.getLogin());
                                            return request;
                                        })
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void shouldNotAuthenticateUserWithExpiredToken() throws Exception {

        mockMvc
                .perform(
                        MockMvcRequestBuilders.get("/me")
                                .header("Authorization",
                                        "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0dXNlckBnbWFpbC5jb20iLCJleHAiOjE2OTUwNjA3Njh9.QF3xBaEK9d5UbKIP_urorQQt9YMZAO7dXKM_sHIIf46yBA7zCyCqX8orts4_cIjYALLUD5YxMhmsg1iwstWKzQ")
                                .with(
                                        request -> {
                                            request.setRemoteUser("testuser@gmail.com");
                                            return request;
                                        })
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    void shouldNotAuthenticateUserWithoutToken() throws Exception {

        mockMvc
                .perform(
                        MockMvcRequestBuilders.get("/me")
                                .header("Authorization",
                                        "Bearer ")
                                .with(
                                        request -> {
                                            request.setRemoteUser("testuser@gmail.com");
                                            return request;
                                        })
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    protected String montarBearer() {
        return "Bearer " + token;
    }

}
