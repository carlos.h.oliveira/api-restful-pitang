package br.cho.eti.pitang.resources;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import br.cho.eti.pitang.services.UserService;
import br.cho.eti.pitang.domain.User;

@SpringBootTest
@AutoConfigureMockMvc
public class AuthResourceSigninTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Test
    public void testSigninEndpoint() throws Exception {
        // Configurar o comportamento esperado do userService mock
        when(userService.signin(any(User.class))).thenReturn("mockedToken");

        mockMvc.perform(MockMvcRequestBuilders.post("/signin")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\":\"testuser\",\"password\":\"testpassword\"}"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.header().string("Authorization", "Bearer mockedToken"));
    }

    @Test
    public void testSigninEndpointFailure() throws Exception {
        // Configurar o comportamento do userService mock para simular uma falha
        when(userService.signin(any(User.class)))
            .thenThrow(new AuthenticationServiceException("Authentication failed"));
    
        mockMvc.perform(MockMvcRequestBuilders.post("/signin")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\":\"testuser\",\"password\":\"testpassword\"}"))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

}
