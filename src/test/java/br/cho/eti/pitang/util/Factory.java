package br.cho.eti.pitang.util;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import br.cho.eti.pitang.domain.Car;
import br.cho.eti.pitang.domain.User;
import br.cho.eti.pitang.dto.CarPostDTO;
import br.cho.eti.pitang.dto.UserPostDTO;

public class Factory {

    private Factory() {
    }

    public static User getUser() {
        User user = User.builder()
                .birthday(LocalDate.of(1984, 4, 4))
                .email("testuser@gmail.com")
                .firstName("Test")
                .lastName("User")
                .login("testuser@gmail.com") // Use "testuser@gmail.com" para corresponder ao login no teste
                .password("123456")
                .phone("81998892223")
                .build();
        user.setId(UUID.randomUUID().toString());

        Car car = Car.builder()//
                .color("White")//
                .licensePlate("PPP-0123")//
                .model("Renault")//
                .user(user)//
                .yearBuild(2018)//
                .build();
        car.setId(UUID.randomUUID().toString());

        user.setCars(new HashSet<>(Arrays.asList(car)));

        return user;
    }

    public static UserPostDTO createValidUserPostDTO() {
        User source = getUser();

        return transform(source);
    }

    public static UserPostDTO getUserPostDTOFromCreatedUser(User source) {
        return transform(source);
    }

    private static UserPostDTO transform(User source) {
        UserPostDTO target = new UserPostDTO();

        target.setBirthday(source.getBirthday());
        target.setEmail(source.getEmail());
        target.setFirstName(source.getFirstName());
        target.setLastName(source.getLastName());
        target.setLogin(source.getLogin());
        target.setPassword(source.getPassword());
        target.setPhone(source.getPhone());

        Set<CarPostDTO> cars = source.getCars().stream().map(c -> {
            CarPostDTO targetCar = new CarPostDTO();
            targetCar.setColor(c.getColor());
            targetCar.setLicensePlate(c.getLicensePlate());
            targetCar.setModel(c.getModel());
            targetCar.setYearBuild(c.getYearBuild());

            return targetCar;
        }).collect(Collectors.toSet());

        target.setCars(cars);
        return target;
    }
}
