# API RESTful para Sistema de Usuários de Carros

## User Stories

### 1 - Como USUÁRIO, preciso ter acesso aos meus dados cadastrados no sistema.

#### Critérios de aceitação:

- É preciso estar cadastrado previamente
- A rota para o acesso deve ser a `api/signin`
- A senha não deve ser exibida
- A senha deve estar criptorafada no banco
- Os usuários serão ordenados por ordem decrescente de número de vezes que seus carros cadastrados foram consultados
- Os carros devem ser exibidos levando em consideração o número de vezes que um carro foi consultado em ordem decrescente e o critério de desempate será o seu modelo em ordem crescente
- Será exibida a data de cadastro do usuário e a data do último login feito pelo usuário
- Para ter acesso, é preciso informar o *login* e a *senha*

### 2 - Como USUÁRIO, preciso me cadastrar no sistema.

#### Critérios de aceitação:

- Não é preciso estar cadastrado previamente
- A rota para o cadastro deve ser a `api/users`
- Os campos necessários para o cadastro:

```json
{ 
   "firstName":"Hello",
   "lastName":"World",
   "email":"hello@world3.com",
   "birthday":"1990-05-01",
   "login":"hello.world",
   "password":"h3ll0",
   "phone":"988888888",
   "cars":[ 
      { 
         "year":2018,
         "licensePlate":"PDV-0265",
         "model":"Audi",
         "color":"White"
      }
   ]
}
```

### 3 - Como USUÁRIO, preciso ter acesso às funcionalidades (consultar, atualizar e remover) relacionadas ao USUÁRIO.

#### Critérios de aceitação:

- Não é preciso estar cadastrado previamente
- A rota para a consulta de usuários deve ser a `api/users`
- A rota para a consulta de um usuário deve ser a `api/users/{id}`
- A rota para a atualização de usuário deve ser a `api/users/{id}`
- A rota para a remoção de usuário deve ser a `api/users/{id}`
- A rota para o upload de um usuário deve ser a `api/users/upload/{id}`

### 4 - Como USUÁRIO, preciso ter acesso às funcionalidades (consultar, cadastrar, atualizar e remover) relacionadas ao CARRO e ao USUÁRIO logado.

#### Critérios de aceitação:

- É preciso estar cadastrado previamente
- A rota para a consulta de carros deve ser a `api/cars` e devem ser exibidos levando em consideração o número de vezes que um carro foi consultado em ordem decrescente e o critério de desempate será o seu modelo em ordem crescente
- A rota para a consulta de um carro deve ser a `api/cars{id}`
- A rota para a remoção de um carro deve ser a `api/cars{id}`
- A rota para a atualização de um carro deve ser a `api/cars{id}`
- A rota para o upload de um carro deve ser a `api/cars/upload/{id}`
- Os campos necessários para o cadastro:

```json
{ 
    "year":2018,
    "licensePlate":"PDV-0265",
    "model":"Audi",
    "color":"White"
}
```

## Solução

Os principais argumentos ao uso do Spring Boot para o desenvolvimento da aplicação é o modo de configuração od seu container de injeção de dependências, inversão de controle usando anotações e a facilidade de integrações entre as dependências, as quais têm a versão atrelada ao parente do projeto.
O Spring Boot é mais simples e direto, sem propor novas soluções para problemas já resolvidos, mas sim alavancando as tecnologias existentes presentes no ecossistema Spring de modo a aumentar significativamente a produtividade do desenvolvedor.

## Build

Por ser um projeto *MAVEN*, qualquer *IDE* que tenha suporte para tal, pode ser utilizada. Após o projeto já ter sido carregado na ide, os comandos abaixos devem ser utilizados:

```sh
$ mvn clean install
```

Acessar a aplicação no endereço: `http://localhost:8080` e suas rotas descritas nas estórias.

## Testes

Os testes unitários estão dentro do pacote `src/test/java` e podem ser executados utilizando os comandos:

```sh
$ mvn test
```

## Swagger
Para acessar o swagger, acessar o link: `https://api-pitang-472a88651f27.herokuapp.com/api/swagger-ui.html`

## Deploy

O deploy foi feito utilizado o [Heroku](https://www.heroku.com/) e a aplicação deve ser acessada no seguinte endereço: `https://api-pitang-472a88651f27.herokuapp.com/api/users`
